# 🦊 Fun with Gitlab-CI

👉 A project to play with a lot of tools within Gitlab-CI.

👉 Based on a very simple `Kotlin` structure to have something to build 😅

📝 Reports generated are available at [fun-with-gitlab-ci.yodamad.fr](https://fun-with-gitlab-ci.yodamad.fr/)

----
## ☝️ Init

Project was initialized with command `gradle init --dsl kotlin --type kotlin-application`

----
## 🪜 Steps

### 🔨 build

* **👨‍🏭 compile** : compilation of Kotlin sources runnning `gradle --build-cache assemble`
* **🔋 download_licenses** : based on [license-gradle-plugin](https://github.com/hierynomus/license-gradle-plugin), download licenses files of dependencies for later analysis

### 🛂 check

* **🙏 unit_tests** : run `gradle check` to generate [jacoco](https://docs.gradle.org/current/userguide/jacoco_plugin.html) reports to provide tests coverage

### 🎨 quality

* **🌤 coverage_badge** : generates information to display the coverage badge on project homepage 
* **💍 sonarcloud** : based on [sonarqube-plugin](https://docs.sonarqube.org/latest/analysis/scan/sonarscanner-for-gradle/), run `gradle sonarqube` to analyse and publish on [sonarcloud](https://sonarcloud.io/dashboard?id=fun-with-gitlab-ci)

### 🚔 security

* **🏘 dependencies_with_🐘** : based on [dependency-check-gradle](https://github.com/jeremylong/dependency-check-gradle), analyses project dependencies for known CVEs
* **🏘 dependencies_with_🦊** : based on [dependency-check-cli](https://github.com/jeremylong/DependencyCheck), analyses project dependencies for known CVEs and publish a report
* **📇 licenses_check** : based on [license-finder](https://github.com/pivotal/LicenseFinder), checks that project doesn't contain unauthorized dependencies based on their license information

### 🐳 docker

* **🦊 deploy_to_gitlab**: based on [jib](https://github.com/GoogleContainerTools/jib), deploy a 🐳 docker image to gitlab registry
* **🐳 deploy_to_dockerhub**: based on [jib](https://github.com/GoogleContainerTools/jib), deploy a 🐳 docker image to the [DockerHub](https://hub.docker.com/)
* **🏗 deploy_with_kaniko**: based on [kaniko](https://github.com/GoogleContainerTools/kaniko), deploy a 🐳 docker image to gitlab registry

### deploy

⚠️ This step cannot be rename, it's a recognized for gitlab to be able to deploy elements to Gitlab Pages

* **pages**: downloads artefacts from previous steps and publish them
