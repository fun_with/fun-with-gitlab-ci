import org.gradle.api.JavaVersion.VERSION_11

plugins {
    // Apply the Kotlin JVM plugin to add support for Kotlin.
    id("org.jetbrains.kotlin.jvm") version "1.6.21"
    // Analyze coverage
    jacoco
    // Quality control
    id("org.sonarqube") version "3.3"
    // Dependencies analysis
    id("org.owasp.dependencycheck") version "7.1.0.1"
    // To be able to download licenses
    id("com.github.hierynomus.license") version "0.15.0"
    // To build Docker image
    id("com.google.cloud.tools.jib") version "3.2.1"
}

group = "fr.yodamad"
version = "1.1.0-SNAPSHOT"
java.sourceCompatibility = VERSION_11
java.targetCompatibility = VERSION_11
kotlin.target { VERSION_11 }

repositories {
    jcenter()
    mavenCentral()
}

dependencies {
    // Align versions of all Kotlin components
    implementation(platform("org.jetbrains.kotlin:kotlin-bom"))

    // Use the Kotlin JDK 8 standard library.
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    // Use the Kotlin test library.
    testImplementation("org.jetbrains.kotlin:kotlin-test")

    // Use the Kotlin JUnit integration.
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit")
}

/// Plugins configuration

// Test coverage
val jacocoXmlReport = "${buildDir}/reports/jacoco/xml/jacoco.xml"
tasks.test {
    finalizedBy(tasks.jacocoTestReport) // report is always generated after tests run
}
tasks.jacocoTestReport {
    dependsOn(tasks.test) // tests are required to run before generating the report
    reports {
        // Enable CSV for badge on project
        csv.isEnabled = true
        csv.destination = file("${buildDir}/reports/jacoco/csv/jacoco.csv")

        // Enable XML to convert to cobertura format for MR display -> TODO
        xml.isEnabled = true
        xml.destination = file(jacocoXmlReport)

        // Enable HTML to publish on Pages -> TODO
        html.destination = file("${buildDir}/reports/jacoco/html")
    }
}

// Sonarqube analysis
sonarqube {
    properties{
        property("sonar.host.url", "https://sonarcloud.io")
        property("sonar.tests", "${project.projectDir}/src/test/kotlin/")
        property("sonar.java.coveragePlugin", "jacoco")
        property("sonar.coverage.jacoco.xmlReportPaths", jacocoXmlReport)
    }
}

// Need for licence download
configurations {
    // Duplicate for other key words if need, such as compileOnly
    implementation.configure {
        isCanBeResolved = true
    }
}
// Download licenses for license checks
downloadLicenses {
    includeProjectDependencies = true
    dependencyConfiguration = "implementation"
}
license {
    ignoreFailures = true
}
